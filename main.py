import pygame as pg
import sys

pg.init()
pg.font.init()

display_width = 625
display_height = 514
pale_blue = (189, 212, 249)
font = pg.font.SysFont('Comic Sans MS', 65, True)
board_img = pg.image.load("board.png")
piece_img = [pg.image.load("green_piece.png"), pg.image.load("orange_piece.png")]
play_img = [pg.image.load("play.png"), pg.image.load("play_pressed.png")]
icon = pg.image.load('logo.png')
pg.display.set_icon(icon)

clock = pg.time.Clock()


gameDisplay = pg.display.set_mode((display_width, display_height))
pg.display.set_caption("Connect Four")


class Piece(pg.sprite.Sprite):

    def __init__(self, x, y, color):
        super().__init__()
        self.image = color
        self.rect = self.image.get_rect()
        self.x = x
        self.y = y
        self.rect.center = (self.x, self.y)
        self.speedy = 3
        self.mask = pg.mask.from_surface(self.image)

    def update(self):
        if self.rect.bottom >= display_height-32:
            self.speedy = 0
        self.rect.bottom += self.speedy

    def collide(self, sprite_group):
        if pg.sprite.spritecollide(self, sprite_group, False, pg.sprite.collide_mask):
            self.speedy = 0


class Board:

    def __init__(self):
        self.piece_group = [pg.sprite.Group() for _ in range(7)]
        self.piece_sprites = []
        self.array = [["NA" for _ in range(7)] for _ in range(6)]
        self.divisions = []
        x1, x2 = 33, 114
        for i in range(7):
            if i == 0:
                self.divisions.append((x1, x2))
            else:
                x1 += 81
                x2 += 81
                self.divisions.append((x1, x2))

    def check_pos(self):
        coord = False
        section = False
        mx, my = pg.mouse.get_pos()
        for x1, x2 in self.divisions:
            if x1 < mx < x2:
                coord = (x1+x2)/2
                section = self.divisions.index((x1, x2))
        return coord, section

    def make_move(self, turn):
        mx, section = self.check_pos()
        group_len = len(self.piece_group[section].sprites())
        if mx and group_len < 6:
            self.piece_sprites.append(Piece(mx, 0, piece_img[turn]))
            self.piece_group[section].add(self.piece_sprites[-1])
            self.array[group_len][section] = turn
            return True
        return False


def check_win(array):

    def inside_board(x, y):
        return 0 <= x <= 5 and 0 <= y <= 6

    win = False
    tie = True
    initial_piece = 'NA'
    for i in range(6):
        for j in range(7):
            for x_dir, y_dir in [[0, 1], [1, 0], [1,1], [1, -1]]:
                x, y = i, j
                initial_piece = array[x][y]
                x += x_dir
                y += y_dir
                count = 0
                if initial_piece == 'NA':
                    break
                while inside_board(x, y):
                    # print("{}, {}", x, y)
                    if array[x][y] == initial_piece:
                        count += 1
                    elif array[x][y] != initial_piece:
                        break
                    x += x_dir
                    y += y_dir
                if count == 3:
                    win = True
                    tie = False
                    return win, tie, initial_piece
    for i in range(6):
        for j in range(7):
            if array[i][j] == 'NA':
                tie = False

    return win, tie, initial_piece


def win_screen(tie, piece):
    translucent_surface = pg.Surface((524, 140), pg.SRCALPHA)
    translucent_surface.fill((255, 255, 255, 200))
    gameDisplay.blit(translucent_surface, (50, 50))
    color = [(0, 219, 106), (247, 148, 29)]
    image_rect = play_img[0].get_rect()
    image_rect.center = (display_width//2, 153)
    gameDisplay.blit(play_img[0], image_rect)

    if tie:
        text = font.render("It's a tie!", True, (255, 48, 82))
    else:
        text = font.render("Winner!", True, color[piece])

    text_rect = text.get_rect()
    text_rect.center = 250, 35
    while True:
        text_surface = pg.Surface((524, 140), pg.SRCALPHA)
        text_surface.fill((255, 255, 255, 0))
        text_surface.blit(text, text_rect)
        gameDisplay.blit(text_surface, (50, 50))
        for event in pg.event.get():
            if event.type == pg.QUIT:
                sys.exit()
            if event.type == pg.MOUSEBUTTONDOWN:
                mouse = pg.mouse.get_pos()
                if image_rect.collidepoint(mouse):
                    gameDisplay.blit(play_img[1], image_rect)
            if event.type == pg.MOUSEBUTTONUP:
                mouse = pg.mouse.get_pos()
                gameDisplay.blit(play_img[0], image_rect)
                if image_rect.collidepoint(mouse):
                    game_loop()

        pg.display.update()


def game_loop():

    game_over = False
    show_board = True
    wait = True
    g = 0  # Green
    o = 1  # Orange
    turn = g
    board = Board()
    win = False
    tie = False
    end = False
    indicator_surface = pg.Surface((80, display_height), pg.SRCALPHA)
    indicator_surface.fill((255, 255, 255, 110))
    while not game_over:
        gameDisplay.fill(pale_blue)
        coord, section = board.check_pos()
        if coord and not end:
            gameDisplay.blit(indicator_surface, (coord - 40, 0))
        for i in range(len(board.piece_group)):

            for piece in board.piece_group[i]:
                board.piece_group[i].remove(piece)
                piece.collide(board.piece_group[i])
                board.piece_group[i].add(piece)
            board.piece_group[i].update()
            board.piece_group[i].draw(gameDisplay)
        indicator_rect = piece_img[turn].get_rect()
        if show_board:
            gameDisplay.blit(board_img, (0, 2))
        if coord and not end:
            indicator_rect.center = (coord, -10)
            gameDisplay.blit(piece_img[turn], indicator_rect)
        if end:
            game_over = True
        for event in pg.event.get():
            if event.type == pg.QUIT:
                sys.exit()
            if event.type == pg.MOUSEBUTTONDOWN and wait:
                if not win and not tie:
                    win, tie, win_piece = check_win(board.array)
                    if board.make_move(turn):
                        if turn == o:
                            turn = g
                        else:
                            turn = o
                        wait = False
                        pg.time.set_timer(pg.USEREVENT+1, 300)

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_b:
                    show_board = not show_board
                if event.key == pg.K_a:
                    for i in range(6):
                        print(board.array[i])
                    print("--------------------")
            if event.type == pg.USEREVENT+1:
                wait = True
        pg.display.update()
        if not win and not tie:
            win, tie, win_piece = check_win(board.array)
        if win or tie:
            if board.piece_sprites[-1].speedy == 0:
                end = True

    win_screen(tie, win_piece)



game_loop()
